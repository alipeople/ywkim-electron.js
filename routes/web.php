<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Events\Chat;
use Carbon\Carbon;

Route::get('/', function (Request $request) {
  if (! $request->session()->has('nickname')) {
    return Inertia::render('User');
  }

  return Inertia::render('Home');
})->name('home');

Route::post('/nickname', function (Request $request) {
  $request->session()->put('nickname', $request->input('nickname'));

  return redirect()->back();
});

Route::post('/chat', function (Request $request) {
  Chat::dispatch(array_merge(['session_id' => $request->session()->getId()], ['nickname' => $request->session()->get('nickname')], ['created_at' => Carbon::now()->format('Y-m-d H:i:s')], ['order' => microtime(true)], $request->only('content')));

  return redirect()->back();
});
