<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Chat implements ShouldBroadcast
{
  use Dispatchable, InteractsWithSockets, SerializesModels;

  public $chat;

  /**
   * Create a new event instance.
   *
   * @return void
   */
  public function __construct(Array $chat)
  {
    $this->chat = [
      'session_id' => $chat['session_id'],
      'nickname' => $chat['nickname'],
      'content' => $chat['content'],
      'created_at' => $chat['created_at'],
      'order' => $chat['order'],
    ];
  }

  /**
   * Get the channels the event should broadcast on.
   *
   * @return \Illuminate\Broadcasting\Channel|array
   */
  public function broadcastOn()
  {
    return new Channel('chat');
  }
}
