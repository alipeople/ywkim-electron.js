const { app, BrowserWindow } = require('electron')
const { spawn } = require('child_process').spawn
const path = require('path')

function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

//  win.loadFile('index.html')
  win.loadURL('http://localhost:80');
}

app.commandLine.appendSwitch('--explicitly-allowed-ports', '80');

app.whenReady().then(() => {
  spawn(path.join(__dirname, 'php\\php.exe'), ['-S', 'localhost:80', '-t', path.join(__dirname, 'app')])

  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
