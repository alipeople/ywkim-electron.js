const mix = require('laravel-mix')

const postCssPlugins = [
  require('postcss-import')(),
  require('postcss-preset-env')({
    stage: 0,
  }),
  require('tailwindcss')('tailwind.config.js'),
]

require('laravel-mix-svelte')

mix.webpackConfig({
  resolve: {
    alias: {
      '@': require('path').resolve('resources/js'),
    },
  },
  plugins: [
    new (require('compression-webpack-plugin')),
  ],
  output: {
    chunkFilename: 'js/[name].js?id=[chunkhash]',
  },
}).js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', postCssPlugins).svelte({
  dev: ! mix.inProduction(),
  emitCss: true,
  preprocess: require('svelte-preprocess')({
    postcss: {
      plugins: postCssPlugins,
    },
  }),
})

if (mix.inProduction()) {
  mix.version()
} else {
  mix.sourceMaps().browserSync({
    proxy: 'chat.test',
    open: false
  })
}
