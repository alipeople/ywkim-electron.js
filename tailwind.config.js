const { colors } = require('tailwindcss/defaultTheme')

module.exports = {
  purge: ['resources/js/**/*.svelte'],
  theme: {
    extend: {
      colors: {
        primary: colors.blue,
        secondary: colors.yellow,
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
